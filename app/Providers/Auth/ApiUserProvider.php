<?php

namespace App\Providers\Auth;

use PessoaService;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Contracts\Auth\Authenticatable;

class ApiUserProvider implements UserProvider
{
    public function retrieveById($identifier) {
        return PessoaService::find($identifier);
    }

    public function retrieveByToken($identifier, $token) {
        return null;
    }

    public function updateRememberToken(Authenticatable $user, $token) {
        return null;
    }

    public function retrieveByCredentials(array $credentials) {
        return PessoaService::findByIdentifier($credentials["identificacao"]);

    }

    public function validateCredentials(Authenticatable $user, array $credentials) {

        $isValidCredentials = PessoaService::checkCredentials($credentials);

        return $isValidCredentials;

    }
}

?>

